#! /usr/bin/env python
# encoding: utf-8


class OutOfPlaygroundError(Exception):
    pass

class PositionAlreadyFilledError(Exception):
    pass

class TicTacToeGame(object):
    playground_size = 3
    playground = None

    def init_playground_size(self, input):
        """
        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.playground
        [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]

        >>> game = TicTacToeGame()
        >>> game.init_playground_size('2')
        >>> game.playground
        [['.', '.'], ['.', '.']]
        """
        input = int(input)
        playground = []
        for i in range(input):
            row = []
            for y in range(input):
                row.append('.')
            playground.append(row)
        self.playground = playground
        self.playground_size = input

    def set_position(self, position, player):
        """
        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((1,1), 'X')
        >>> game.playground
        [['X', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]

        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((1,1), 'O')
        >>> game.playground
        [['O', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]

        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((1,1), 'O')
        >>> game.set_position((1,1), 'X')
        Traceback (most recent call last):
        ...
        PositionAlreadyFilledError

        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((1,1), '.')
        Traceback (most recent call last):
        ...
        InvalidInputError

        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((1,4), 'X')
        Traceback (most recent call last):
        ...
        OutOfPlaygroundError

        >>> game = TicTacToeGame()
        >>> game.init_playground_size(3)
        >>> game.set_position((0,1), 'O')
        Traceback (most recent call last):
        ...
        OutOfPlaygroundError
        """
        x, y = position
        if (0 >= x or x > self.playground_size or
            0 >= y or y > self.playground_size):
            raise OutOfPlaygroundError

        if self.playground[x-1][y-1] != '.':
            raise PositionAlreadyFilledError

        self.playground[x-1][y-1] = player

    def check_playground(self):
        """ Determine if the game is finished
        """
        pass

    def run(self):
        print "Welcome to TDD's TicTacToe!"
        print "==========================="
        size_input = raw_input("Please enter your playground size: ")
        #self.init_playground(size)

if __name__ == '__main__':
    import nose
    nose.runmodule(argv=['-vv', '--with-doctest'])
    game = TicTacToeGame()
    game.run()
